/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.calcite.jdbc;

import org.apache.calcite.avatica.ColumnMetaData;
import org.apache.calcite.avatica.NoSuchStatementException;
import org.apache.calcite.schema.StreamableTable;
import org.apache.calcite.server.CalciteServerStatement;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CalciteStreamMetaImpl extends CalciteMetaImpl {

//  String whereJoin = String.valueOf("ss2.people.IDPEOPLE = GITREV.T.ID "
//      + "and ss.DEPTS.DEPTNO=ss2.people.IDPEOPLE").toLowerCase(Locale.ROOT);

  String whereJoin = "";
  HashMap<Integer, ColumnMetaData> columnGlobalId = new HashMap<>();

  final int columnMaxSize = Integer.MIN_VALUE;

  public CalciteStreamMetaImpl(final CalciteConnectionImpl connection) {
    super(connection);
  }

  @Override public ExecuteResult prepareAndExecute(StatementHandle h,
      String sql, long maxRowCount, int maxRowsInFirstFrame,
      PrepareCallback callback) throws NoSuchStatementException {

    long currentOffset = 0;

    CalcitePrepare.CalciteSignature<Object> signature = null;

    whereJoin = "";

    String[] vSql = splitQuery(sql, getConnection().server.getStatement(h).createPrepareContext());

    ExecuteResult globalMeta = null;
    List globalList = new ArrayList();
    List<ColumnMetaData> globalColumnList = new ArrayList<>();
    HashMap<ColumnMetaData, Integer> columnListSizes = new HashMap<>();
    //STREAM
    for (int i = 0; i < 2; i++) {

      globalList = new ArrayList();
      globalColumnList = new ArrayList<>();

      HashMap<Integer, HashMap<String, Queue>> columnNameValue = new HashMap<>();
      HashMap<Integer, HashMap<String, Integer>> columnIdValue = new HashMap<>();
      HashMap<Integer, Queue<Object>> globalListResults = new HashMap<>();
      columnGlobalId = new HashMap<>();

      ArrayList<Thread> threads = new ArrayList<>();

      for (int j = 0; j < vSql.length; j++) {

        sql = vSql[j];

        final CalciteConnectionImpl calciteConnection;

        synchronized (callback.getMonitor()) {
          try {
            callback.clear();
          } catch (SQLException throwables) {
            throwables.printStackTrace();
          }
          calciteConnection = getConnection();
          final CalciteServerStatement statement =
              calciteConnection.server.getStatement(h);
          final CalcitePrepare.Context context = statement.createPrepareContext();
          final CalcitePrepare.Query<Object> query = super.toQuery(context, sql);
          signature = calciteConnection.parseQuery(query, context, maxRowCount);
          statement.setSignature(signature);
          final int updateCount;
          switch (signature.statementType) {
          case CREATE:
          case DROP:
          case ALTER:
          case OTHER_DDL:
            updateCount = 0; // DDL produces no result set
            break;
          default:
            updateCount = -1; // SELECT and DML produces result set
            break;
          }
          try {
            callback.assign(signature, null, updateCount);
          } catch (SQLException throwables) {
            throwables.printStackTrace();
          }
        }

        Consulta classeConsulta = new Consulta(calciteConnection, sql, callback, signature,
            maxRowCount, globalList, h, currentOffset, globalColumnList, columnNameValue,
            columnIdValue, columnListSizes, globalListResults, vSql, j);

        Thread t = new Thread(classeConsulta);
        t.start();
        threads.add(t);


//          executaQueries(sql, callback, signature, maxRowCount, globalList, h, currentOffset,
//              globalColumnList, columnNameValue, columnIdValue, columnListSizes,
//              globalListResults, vSql, j);
      }

      try {
        for (Iterator<Thread> it = threads.iterator(); it.hasNext();) {
          Thread t = it.next();
          t.join();
        }
      } catch (InterruptedException ex) {
        System.out.println("Puxa, estava dormindo! Você me acordou");
      }

      //Faz join dos dados
      globalList = (List) executeJoin(globalListResults, globalColumnList,
          columnNameValue, columnIdValue, vSql, columnListSizes);
      imprimirCollection((Queue) globalList, globalColumnList, columnIdValue,
          columnListSizes);
    }

    ExecuteResult result = new ExecuteResult(globalList);

//      organizaRetorno(getConnection().server.getStatement(h), callback,
//          globalList, globalColumnList);
    return result;
    // TODO: share code with prepare and createIterable
  }

  private void executaQueries(String sql, PrepareCallback callback,
      CalcitePrepare.CalciteSignature<Object> signature,
      long maxRowCount, List globalList,
      StatementHandle h, long currentOffset,
      List<ColumnMetaData> globalColumnList,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes,
      HashMap<Integer, Queue<Object>> globalListResults,
      String [] vSql, Integer idQuery)
                  throws SQLException, NoSuchStatementException {

    synchronized (callback.getMonitor()) {
      callback.clear();
      final CalciteConnectionImpl calciteConnection = getConnection();
      final CalciteServerStatement statement =
          calciteConnection.server.getStatement(h);
      final CalcitePrepare.Context context = statement.createPrepareContext();
      final CalcitePrepare.Query<Object> query = super.toQuery(context, sql);
      signature = calciteConnection.parseQuery(query, context, maxRowCount);
      statement.setSignature(signature);
      final int updateCount;
      switch (signature.statementType) {
      case CREATE:
      case DROP:
      case ALTER:
      case OTHER_DDL:
        updateCount = 0; // DDL produces no result set
        break;
      default:
        updateCount = -1; // SELECT and DML produces result set
        break;
      }
      callback.assign(signature, null, updateCount);
    }

    ExecuteResult meta = null;

    callback.execute();
    final MetaResultSet metaResultSet =
        MetaResultSet.create(h.connectionId, h.id, false, signature, null);

    meta = new ExecuteResult(ImmutableList.of(metaResultSet));

    globalList.addAll(ImmutableList.of(metaResultSet));

    Frame f = super.fetchForceReload(h, currentOffset, -1);

    globalColumnList.addAll(signature.columns);

    //inicializar lista de colunas com listas vazias
    inicializarListaColunas(signature.columns, columnNameValue, idQuery,
        columnIdValue, columnListSizes);

    Iterable<Object> rows = f.rows;
    globalListResults.put(idQuery, new LinkedList((Collection) rows));

    Iterator it = rows.iterator();
    //System.out.println(globalColumnList);
    for (; it.hasNext();) {
      Object row = it.next();
      ArrayList rowArray = new ArrayList((Collection) row);
      //para cada coluna
      for (int col = 0; col < rowArray.size(); col++) {
        ColumnMetaData columnMetaData = signature.columns.get(col);
        String key = getKeyTable(columnMetaData);
        columnNameValue.get(idQuery).get(key).add(rowArray.get(col));
        columnIdValue.get(idQuery).put(key, col);

        if (vSql.length == 1) {
          columnGlobalId.put(col, columnMetaData);
        }

        //System.out.print(rowArray.get(col) + " | ");
      }
      //System.out.println();
    }

    currentOffset++;

    //globalMeta = meta;
  }


  private Queue executeJoin(HashMap<Integer, Queue<Object>> globalListResults,
      List<ColumnMetaData> globalColumnList,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      String[] vSql, HashMap<ColumnMetaData, Integer> columnListSizes) {

    Queue joins = new LinkedList();

    if (whereJoin != null && !whereJoin.equals(String.valueOf(""))) {

      String[] wheres = whereJoin.split("and");

      for (int i = 0; i < wheres.length; i++) {
        String nomeColuna01 = wheres[i].split("=")[0].trim();
        String nomeColuna02 = wheres[i].split("=")[1].trim();

        int idQuery01 = getIdQueryByColumnName(nomeColuna01, columnNameValue);
        int idQuery02 = getIdQueryByColumnName(nomeColuna02, columnNameValue);

        Iterator<Object> first =
            globalListResults.get(idQuery01).iterator();
        Iterator<Object> second =
            globalListResults.get(idQuery02).iterator();

        joins = join(first, second, globalColumnList, columnNameValue,
            idQuery01, idQuery02,
            nomeColuna01, nomeColuna02, columnIdValue, columnListSizes);

        globalListResults.remove(idQuery01);
        globalListResults.remove(idQuery02);
        globalListResults.put(globalListResults.size() + 1, joins);

        HashMap<String, Queue> colls = new HashMap<>();
        colls.putAll(columnNameValue.get(idQuery01));
        colls.putAll(columnNameValue.get(idQuery02));
        columnNameValue.remove(idQuery01);
        columnNameValue.remove(idQuery02);
        columnNameValue.put(columnNameValue.size() + 1, colls);

        HashMap<String, Integer> col01 = columnIdValue.get(idQuery01);
        HashMap<String, Integer> col02 = columnIdValue.get(idQuery02);

        HashMap<String, Integer> newCollIdValue = new HashMap<>();
        columnGlobalId.clear();
        for (int j = 0; j < col01.size(); j++) {
          String key = (String) col01.keySet().toArray()[j];
          Integer value = col01.get(key);
          columnGlobalId.put(value, getColumnMetaDataByKeyTable(key, globalColumnList));
          newCollIdValue.put(key, value);
        }

        for (int j = 0; j < col02.size(); j++) {
          String key = (String) col02.keySet().toArray()[j];
          Integer value = col02.get(key);
          columnGlobalId.put(value + col01.size(),
              getColumnMetaDataByKeyTable(key, globalColumnList));
          newCollIdValue.put(key, value + col01.size());
        }

        columnIdValue.remove(idQuery01);
        columnIdValue.remove(idQuery02);
        columnIdValue.put(columnIdValue.size() + 1, newCollIdValue);
      }
    }

    //Fazer JOIN com as outras consultas que nao fazem parte do WHERE
    //Fazer full join entre o resultado da consulta e aquelas que nao fazem parte do innerjoin
    if (globalListResults.size() > 1) {
      Object[] queries = globalListResults.keySet().toArray();
      Collection c1 = globalListResults.get(queries[0]);

      HashMap<String, Integer> col01 = columnIdValue.get(queries[0]);

      for (int i = 1; i < queries.length; i++) {
        Collection c2 = globalListResults.get(queries[i]);

        HashMap<String, Integer> col02 = columnIdValue.get(queries[i]);

        c1 = fullJoin(new ArrayList(c1), new ArrayList(c2));



        HashMap<String, Integer> newCollIdValue = new HashMap<>();
        columnGlobalId.clear();
        for (int j = 0; j < col01.size(); j++) {
          String key = (String) col01.keySet().toArray()[j];
          Integer value = col01.get(key);
          columnGlobalId.put(value, getColumnMetaDataByKeyTable(key, globalColumnList));
          newCollIdValue.put(key, value);
        }

        for (int j = 0; j < col02.size(); j++) {
          String key = (String) col02.keySet().toArray()[j];
          Integer value = col02.get(key);
          columnGlobalId.put(value + col01.size(),
              getColumnMetaDataByKeyTable(key, globalColumnList));
          newCollIdValue.put(key, value + col01.size());
        }
        joins.clear();
        joins.addAll(c1);
      }
      return joins;
    } else {
      //Caso nao tenha clausula WHERE entao faz o Join
      HashMap<String, Integer> col01 = columnIdValue.get(columnIdValue.keySet().iterator().next());
      HashMap<String, Integer> newCollIdValue = new HashMap<>();
      columnGlobalId.clear();
      for (int j = 0; j < col01.size(); j++) {
        String key = (String) col01.keySet().toArray()[j];
        Integer value = col01.get(key);
        columnGlobalId.put(value, getColumnMetaDataByKeyTable(key, globalColumnList));
        newCollIdValue.put(key, value);
      }
      return new LinkedList(globalListResults.entrySet().iterator().next().getValue());
    }

//     else if (vSql.length > 1) {
//      Object[] queries = globalListResults.keySet().toArray();
//      Collection c1 = globalListResults.get(queries[0]);
//      for (int i = 1; i < queries.length; i++) {
//        Collection c2 = globalListResults.get(queries[i]);
//
//        c1 = fullJoin(new ArrayList(c1), new ArrayList(c2));
//      }
//      return new LinkedList(c1);
//    }

  }

  private Queue join(Iterator<Object> first, Iterator<Object> second,
      List<ColumnMetaData> globalColumnList,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue,
      int idQuery01, int idQuery02,
      String nomeColuna01, String nomeColuna02,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes) {
    List c = Lists.newArrayList(second);
    Queue joinResult = new LinkedList();
    int idRow01 = 0;
    Boolean joinExecuted = false;
    for (; first.hasNext();) {
      Object rowFirst = first.next();
      ArrayList rowFirstArray = new ArrayList((Collection) rowFirst);

      for (int i = 0; i < c.size(); i++) {
        Object rowSecond = c.get(i);
        ArrayList rowSecondArray = new ArrayList((Collection)  rowSecond);

        if (verifyJoinWhere(rowFirstArray, rowSecondArray, idRow01, i, columnNameValue,
            idQuery01, idQuery02, nomeColuna01, nomeColuna02, columnIdValue)) {
          List newList = (List) Stream.concat(
              (new ArrayList((Collection) rowFirstArray)).stream(),
              (new ArrayList((Collection) rowSecondArray)).stream())
              .collect(Collectors.toList());

          joinResult.add(newList);
          joinExecuted = true;
        }
      }
      idRow01++;
    }

    //adiciona as colunas da query2 na colecao da query1, para viabilizar
    //o proximo JOIN
    if (joinExecuted) {
      columnNameValue.get(idQuery01).putAll(columnNameValue.get(idQuery02));
    }

    //imprimirCollection(joinResult, globalColumnList);
    return joinResult;
  }

  private Queue fullJoin(ArrayList firstColl, ArrayList secondColl) {

    Queue joinResult = new LinkedList();
    int idRow01 = 0;
    Boolean joinExecuted = false;
    Iterator first = firstColl.iterator();
    for (; first.hasNext();) {
      Object rowFirst = first.next();
      ArrayList rowFirstArray = new ArrayList((Collection) rowFirst);

      for (int i = 0; i < secondColl.size(); i++) {
        Object rowSecond = secondColl.get(i);
        ArrayList rowSecondArray = new ArrayList((Collection) rowSecond);

        List newList = (List) Stream.concat(
            (new ArrayList((Collection) rowFirstArray)).stream(),
            (new ArrayList((Collection) rowSecondArray)).stream())
            .collect(Collectors.toList());

        joinResult.add(newList);
      }
    }
    return joinResult;
  }

  private Boolean verifyJoinWhere(ArrayList row01, ArrayList row02, int index01, int index02,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue,
      int idQuery01, int idQuery02,
      String nomeColuna01, String nomeColuna02,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue) {

    int pos01 = columnIdValue.get(idQuery01).get(nomeColuna01);
    int pos02 = columnIdValue.get(idQuery02).get(nomeColuna02);

    return String.valueOf(row01.get(pos01))
        .equals(
            String.valueOf(
                row02.get(pos02)));
  }

  private Integer getIdQueryByColumnName(String columnName,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue) {
    Iterator<Integer> it = columnNameValue.keySet().iterator();
    for (; it.hasNext();) {
      Integer id = it.next();
      HashMap<String, Queue> colls = columnNameValue.get(id);
      if (colls.keySet().contains(columnName)) {
        return id;
      }
    }
    return null;
  }

  private String getKeyTable(ColumnMetaData column) {
    return String.valueOf(column.schemaName + "."
        + column.tableName  + "."
        + column.columnName).toLowerCase(Locale.ROOT);
  }

  private ColumnMetaData getColumnMetaDataByKeyTable(String keyTable,
      List<ColumnMetaData> columns) {
    for (Iterator<ColumnMetaData> it = columns.iterator(); it.hasNext();) {
      ColumnMetaData column = it.next();
      String key = String.valueOf(column.schemaName + "."
          + column.tableName  + "."
          + column.columnName).toLowerCase(Locale.ROOT);
      if (key.equals(keyTable)) {
        return column;
      }
    }
    return null;
  }

  private void inicializarListaColunas(List<ColumnMetaData> columns,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue, int idQuery,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes) {
    for (Iterator<ColumnMetaData> it = columns.iterator(); it.hasNext();) {
      ColumnMetaData metaData = it.next();

      columnListSizes.put(metaData, metaData.columnName.length() + 2);

      String key = String.valueOf(metaData.schemaName + "."
          + metaData.tableName  + "."
          + metaData.columnName).toLowerCase(Locale.ROOT);

      if (columnNameValue.get(idQuery) == null) {
        HashMap<String, Queue> columnValues = new HashMap<>();
        columnValues.put(key, new LinkedList());
        columnNameValue.put(idQuery, columnValues);

        HashMap<String, Integer> columnIds = new HashMap<>();
        columnIdValue.put(idQuery, columnIds);
      } else {
        columnNameValue.get(idQuery).put(key, new LinkedList());
      }
    }
  }

  private String getCompleteColumnName(List<ColumnMetaData> globalColumnList, int idColumn) {
    return getKeyTable(globalColumnList.get(idColumn));
  }

//  private HashMap<String, Integer> getMaxValue(List<ColumnMetaData> globalColumnList) {
//    for (Iterator<ColumnMetaData> it = globalColumnList.iterator(); it.hasNext();) {
//      ColumnMetaData cMetaData = it.next();
//      cMetaData.columnName
//    }
//  }

  private void imprimeLinha(HashMap<ColumnMetaData, Integer> columnListSizes) {
    System.out.println();
    System.out.print("+");

    for (int i = 0; i < columnGlobalId.size(); i++) {
      ColumnMetaData col = columnGlobalId.get(i);
      int tam = columnListSizes.get(col).intValue();
      for (int j = 0; j < tam; j++) {
        System.out.print("-");
      }
      System.out.print("+");
    }
    System.out.println();
  }

  private void imprimirCollection(Queue joinResult,
      List<ColumnMetaData> globalColumnList,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes) {


    for (Iterator it = joinResult.iterator(); it.hasNext();) {
      List l = (List) it.next();
      for (int col = 0; col < l.size(); col++) {
        ColumnMetaData columnMetaData = columnGlobalId.get(col);
        String dado = String.valueOf(l.get(col));
        columnListSizes.put(columnMetaData,
            Integer.max(dado.trim().length() + 2,
                columnListSizes.get(columnMetaData)));
      }
    }

    int tam;

    imprimeLinha(columnListSizes);

    System.out.print("|");
    for (Iterator<Integer> it = columnGlobalId.keySet().iterator(); it.hasNext();) {
      Integer id = it.next();
      ColumnMetaData column = columnGlobalId.get(id);
      tam = columnListSizes.get(column).intValue();
      System.out.print(
          String.format(Locale.ROOT, "%-" + tam + "." + tam + "s",
          " " + column.columnName + " ") + "|");
    }

    imprimeLinha(columnListSizes);

    for (Iterator it = joinResult.iterator(); it.hasNext();) {
      List l = (List) it.next();
      System.out.print("|");
      for (int col = 0; col < l.size(); col++) {

        //Nome da coluna de ID = col?
        ColumnMetaData columnMetaData = columnGlobalId.get(col);
        tam = columnListSizes.get(columnMetaData).intValue();

        if (l.get(col) instanceof String) {
          System.out.print(
              String.format(Locale.ROOT, "%-" + tam + "." + tam + "s",
                  " " + ((String) l.get(col)).
                      replaceAll("[\\t\\n\\r]+", " ").trim() + " ") + "|");
        } else {
          System.out.print(
              String.format(Locale.ROOT, "%-" + tam + "." + tam + "s",
                  " " + String.valueOf(l.get(col)) + " ") + "|");
        }
      }
      if (it.hasNext()) {
        System.out.println();
      } else {
        imprimeLinha(columnListSizes);
      }
    }
  }

  private String[] splitQuery(String sql, CalcitePrepare.Context context) {
    //Split from
    String from = sql.split("from")[1].split("where")[0];

    ArrayList<String> schemaNames = new ArrayList<>();
    ArrayList<String> tableNames = new ArrayList<>();
    HashMap<String, ArrayList<String>> schemaTables = new HashMap<>();
    HashMap<String, Boolean> schemaIsStream = new HashMap<>();
    String[] tables = from.split(",");
    for (int i = 0; i < tables.length; i++) {
      String schema = tables[i].split("\\.")[0].trim();
      String table = tables[i].split("\\.")[1].trim();
      tableNames.add(table);

      if (!schemaNames.contains(schema)) {
        schemaNames.add(schema);
      }

      if (schemaTables.get(schema) == null) {
        schemaTables.put(schema, new ArrayList<String>());
      }

      schemaTables.get(schema).add(table);

    }

    //No caso de um unico SCHEMA, nao precisa quebrar a consulta em varias outras
    if (schemaNames.size() == 1) {
      return new String[]{sql};
    }

    //Guarda a relacao entre o alias das tabelas e o nome das tabelas e schema
    HashMap<String, String> aliasToSchemaTable = new HashMap<>();

    //Guarda a relacao entre o alias das tabelas e o nome do Schema
    HashMap<String, String> aliasToSchema = new HashMap<>();

    //Caso quando ha mais de um SCHEMA
    HashMap<String, String> newSql = new HashMap<>();
    for (Iterator<String> it = schemaNames.iterator(); it.hasNext();) {
      String schema = it.next();
      ArrayList<String> tab = schemaTables.get(schema);
      newSql.put(schema, " from ");

      schemaIsStream.put(schema, false);

      for (int i = 0; i < tab.size(); i++) {

        String tabAliasName = (String) tab.toArray()[i];
        String tabName = tabAliasName.replace("AS", "as").
            split("as")[0].trim().toUpperCase(Locale.ROOT);
        String aliasName = tabAliasName.replace("AS", "as").
            split("as")[1].trim().toUpperCase(Locale.ROOT);

        aliasToSchemaTable.put(aliasName.toLowerCase(Locale.ROOT),
            (schema + "." + tabName).toLowerCase(Locale.ROOT));

        aliasToSchema.put(aliasName.toLowerCase(Locale.ROOT), schema);

        if (context.getDataContext().getRootSchema().
            getSubSchema(schema.toUpperCase(Locale.ROOT)).getTable(tabName)
            instanceof StreamableTable) {
          schemaIsStream.put(schema, true);
        }

        if ((i + 1) < tab.size()) {
          newSql.put(schema, newSql.get(schema) + schema + "." + tabName + " " + aliasName + ",");
        } else {
          newSql.put(schema, newSql.get(schema) + schema + "." + tabName + " " + aliasName);
        }
      }
    }

    //Split select
    String select = sql.split("from")[0];

    if (select.contains("*")) {
      for (Iterator<String> it = schemaNames.iterator(); it.hasNext();) {
        String schemaName = it.next();
        if (schemaIsStream.get(schemaName)) {
          newSql.put(schemaName, "select stream * " + newSql.get(schemaName));
        } else {
          newSql.put(schemaName, "select * " + newSql.get(schemaName));
        }
      }
    }

    String where = "";
    //Split where
    if (sql.contains("where")) {
      where = (sql.split("from")[1].split("where")[1]).toLowerCase(Locale.ROOT);
      HashMap<String, String> schemaWhere = organizaWhere(where,
          aliasToSchema, aliasToSchemaTable);
      if (schemaWhere.size() > 0) {
        for (Iterator<String> it = schemaWhere.keySet().iterator(); it.hasNext();) {
          String schemaName = it.next();
          String filter = schemaWhere.get(schemaName);
          newSql.put(schemaName, newSql.get(schemaName) + " where " + filter);
        }
      }
    } else {
      whereJoin = "";
    }

    String[] aSql = new String[newSql.size()];
    int i = 0;
    for (Iterator<String> it = newSql.keySet().iterator(); it.hasNext();) {
      String key = it.next();
      aSql[i] = newSql.get(key);
      i++;
    }

    return aSql;
  }

  private HashMap<String, String> organizaWhere(String where, HashMap<String, String> alias,
      HashMap<String, String> aliasToSchemaTable) {
    String whereMultiple = "";
    HashMap<String, String> schemaWhere = new HashMap<>();
    //identifica filtros especificos de um esquema
    String[] filters = where.split(" and | or ");
    for (int i = 0; i < filters.length; i++) {
      String filter = filters[i];
      //conta quantos schemas ha no filtro
      int count = 0;
      String schemaName = "";
      for (Iterator<String> it = alias.keySet().iterator(); it.hasNext();) {
        String sAlias = it.next();
        if (filter.contains(sAlias + ".")) {
          count++;
          schemaName = alias.get(sAlias);
        }
      }
      //where com dois schemas
      if (count == 2) {
        whereMultiple += filter + " and ";
      } else if (count == 1) {
        if (schemaWhere.get(schemaName) == null) {
          schemaWhere.put(schemaName, filter);
        } else {
          schemaWhere.put(schemaName, schemaWhere.get(schemaName) + " and "
              + filter);
        }
      }
    }

    if (!whereMultiple.equals("")) {
      //remove last ' and '
      whereMultiple = whereMultiple.substring(0, whereMultiple.length() - 5);
      //padroniza filtros que envolvem dois esquemas
      for (Iterator<String> it = aliasToSchemaTable.keySet().iterator(); it.hasNext();) {
        String sAlias = it.next();
        String tabSchema = aliasToSchemaTable.get(sAlias);
        whereMultiple = whereMultiple.replace(sAlias + ".", tabSchema + ".");
      }
      this.whereJoin = whereMultiple;
    }
    return schemaWhere;
  }
}
