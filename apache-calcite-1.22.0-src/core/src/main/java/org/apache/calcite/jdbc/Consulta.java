/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.calcite.jdbc;

import org.apache.calcite.avatica.ColumnMetaData;
import org.apache.calcite.avatica.Meta;
import org.apache.calcite.avatica.NoSuchStatementException;

import com.google.common.collect.ImmutableList;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

public class Consulta extends CalciteStreamMetaImpl implements Runnable {

  String sql;
  Meta.PrepareCallback callback;
  CalcitePrepare.CalciteSignature<Object> signature;
  long maxRowCount;
  List globalList;
  Meta.StatementHandle h;
  long currentOffset;
  List<ColumnMetaData> globalColumnList;
  HashMap<Integer, HashMap<String, Queue>> columnNameValue;
  HashMap<Integer, HashMap<String, Integer>> columnIdValue;
  HashMap<ColumnMetaData, Integer> columnListSizes;
  HashMap<Integer, Queue<Object>> globalListResults;
  String [] vSql;
  Integer idQuery;
  final CalciteConnectionImpl connection;


  public Consulta(final CalciteConnectionImpl connection, String sql, PrepareCallback callback,
      CalcitePrepare.CalciteSignature<Object> signature,
      long maxRowCount, List globalList,
      StatementHandle h, long currentOffset,
      List<ColumnMetaData> globalColumnList,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes,
      HashMap<Integer, Queue<Object>> globalListResults,
      String [] vSql, Integer idQuery) {
    super(connection);

    this.connection = connection;
    this.sql = sql;
    this.callback = callback;
    this.signature = signature;
    this.maxRowCount = maxRowCount;
    this.globalList = globalList;
    this.h = h;
    this.currentOffset = currentOffset;
    this.globalColumnList = globalColumnList;
    this.columnNameValue = columnNameValue;
    this.columnIdValue = columnIdValue;
    this.columnListSizes = columnListSizes;
    this.globalListResults = globalListResults;
    this.vSql = vSql;
    this.idQuery = idQuery;
  }

  private void executaConsulta() throws SQLException, NoSuchStatementException {
//    synchronized (callback.getMonitor()) {
//      callback.clear();
//      final CalciteConnectionImpl calciteConnection = connection;
//      final CalciteServerStatement statement =
//          calciteConnection.server.getStatement(h);
//      final CalcitePrepare.Context context = statement.createPrepareContext();
//      final CalcitePrepare.Query<Object> query = super.toQuery(context, sql);
//      signature = calciteConnection.parseQuery(query, context, maxRowCount);
//      statement.setSignature(signature);
//      final int updateCount;
//      switch (signature.statementType) {
//      case CREATE:
//      case DROP:
//      case ALTER:
//      case OTHER_DDL:
//        updateCount = 0; // DDL produces no result set
//        break;
//      default:
//        updateCount = -1; // SELECT and DML produces result set
//        break;
//      }
//      callback.assign(signature, null, updateCount);
//    }

    Meta.ExecuteResult meta = null;

    callback.execute();
    final Meta.MetaResultSet metaResultSet =
        Meta.MetaResultSet.create(h.connectionId, h.id, false,
            signature, null);

    meta = new Meta.ExecuteResult(ImmutableList.of(metaResultSet));

    globalList.addAll(ImmutableList.of(metaResultSet));

    Meta.Frame f = super.fetchForceReload(h, currentOffset, -1);

    System.out.println("Executou consulta: " + idQuery);

    globalColumnList.addAll(signature.columns);

    //inicializar lista de colunas com listas vazias
    inicializarListaColunas(signature.columns, columnNameValue, idQuery,
        columnIdValue, columnListSizes);

    Iterable<Object> rows = f.rows;
    globalListResults.put(idQuery, new LinkedList((Collection) rows));

    Iterator it = rows.iterator();
    //System.out.println(globalColumnList);
    for (; it.hasNext();) {
      Object row = it.next();
      ArrayList rowArray = new ArrayList((Collection) row);
      //para cada coluna
      for (int col = 0; col < rowArray.size(); col++) {
        ColumnMetaData columnMetaData = signature.columns.get(col);
        String key = getKeyTable(columnMetaData);
        columnNameValue.get(idQuery).get(key).add(rowArray.get(col));
        columnIdValue.get(idQuery).put(key, col);

        if (vSql.length == 1) {
          columnGlobalId.put(col, columnMetaData);
        }

        //System.out.print(rowArray.get(col) + " | ");
      }
      //System.out.println();
    }

    currentOffset++;

    //globalMeta = meta;

  }

  private void inicializarListaColunas(List<ColumnMetaData> columns,
      HashMap<Integer, HashMap<String, Queue>> columnNameValue, int idQuery,
      HashMap<Integer, HashMap<String, Integer>> columnIdValue,
      HashMap<ColumnMetaData, Integer> columnListSizes) {
    for (Iterator<ColumnMetaData> it = columns.iterator(); it.hasNext();) {
      ColumnMetaData metaData = it.next();

      columnListSizes.put(metaData, metaData.columnName.length() + 2);

      String key = String.valueOf(metaData.schemaName + "."
          + metaData.tableName  + "."
          + metaData.columnName).toLowerCase(Locale.ROOT);

      if (columnNameValue.get(idQuery) == null) {
        HashMap<String, Queue> columnValues = new HashMap<>();
        columnValues.put(key, new LinkedList());
        columnNameValue.put(idQuery, columnValues);

        HashMap<String, Integer> columnIds = new HashMap<>();
        columnIdValue.put(idQuery, columnIds);
      } else {
        columnNameValue.get(idQuery).put(key, new LinkedList());
      }
    }
  }

  private String getKeyTable(ColumnMetaData column) {
    return String.valueOf(column.schemaName + "."
        + column.tableName  + "."
        + column.columnName).toLowerCase(Locale.ROOT);
  }

  @Override public void run() {
    try {
      System.out.println(this.sql);
      this.executaConsulta();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    } catch (NoSuchStatementException e) {
      e.printStackTrace();
    }
  }
}
