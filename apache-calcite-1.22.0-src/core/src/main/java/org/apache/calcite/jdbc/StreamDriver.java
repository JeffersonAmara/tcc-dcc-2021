/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to you under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.calcite.jdbc;

import org.apache.calcite.avatica.AvaticaConnection;
import org.apache.calcite.avatica.Meta;



public class StreamDriver extends GenericDriver {
  public static final String CONNECT_STRING_PREFIX = "jdbc:calcite:";

  static {
    new StreamDriver().register();
  }

  public StreamDriver() {
    super();
  }

  @Override protected String getConnectStringPrefix() {
    return CONNECT_STRING_PREFIX;
  }

  @Override public Meta createMeta(AvaticaConnection connection) {
//    try {
//      Class.forName("org.postgresql.Driver");
//    } catch (ClassNotFoundException e) {
//      System.out.println(e.getMessage());
//      System.out.println(e.getCause());
//      System.out.println(e.getException());
//    }
//
//    DataSource dataSource =
//        JdbcSchema.dataSource(
//          "jdbc:postgresql://172.18.0.2:5432/gitrev?user=postgres&password=OZ9xLrfEoG3fbquxjLBy",
//          "org.postgresql.Driver",
//          "postgres",
//          "OZ9xLrfEoG3fbquxjLBy");
//
//    String query = "SELECT * from public.pullrequest";
//    Connection c = null;
//    try {
//      c = dataSource.getConnection();
//      Statement stmt = c.createStatement();
//      ResultSet rs = stmt.executeQuery(query);
//      while (rs.next()) {
//        String title = rs.getString("title");
//        String body = rs.getString("body");
//        System.out.println(title + "\t" + body);
//      }
//    } catch (SQLException e) {
//      System.out.println(e.getMessage());
//    }
    return new CalciteStreamMetaImpl((CalciteConnectionImpl) connection);
  }
}
