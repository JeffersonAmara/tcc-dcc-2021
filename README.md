## Importar o projeto Calcite no IntelliJ: ##

### Fazer o build do projeto: ###

> Pode ser feito diretamente pelo IntelliJ, através da execução do Gradle. Para fazer isso, os passos são:

- Clicar em Run e selecionar "Run...", depois "Edit configuration";

- Clicar em Gradle e criar um novo build Gradle:

    - Na opção "Gradle Project", selecione o seu projeto Calcite que você importou no IntelliJ

    - Na opção "Tasks", adicione "build", sem aspas!

    - Na opção "Arguments", adicione "-x Test".

    - Deixe as outras opções sem preenchimento.

- Clicar no botão "Run"

> OBS.: Durante o build, caso ocorra o erro 
<pre><code>"Execution failed for task ':core:autostyleJavaCheck'."</code></pre>

> Você deve executar, utilizando o prompt, o seguinte comando na raiz do seu projeto Calcite:

<pre><code> ./gradlew autostyleApply </code></pre>

### Executar o Calcite ###
- Adicinar uma execução (Run) no IntelliJ (Mesmo processo feito anteriormente)

- Adicionar "Jar Application"

- "Path to JAR": /home/amara/calcite/apache-calcite-1.22.0-src/example/csv/build/libs/sqllineClasspath.jar

> (Observar se o build gerou o jar corretamente nessa pasta)

- "VM options": <pre><code>-Xmx1g -Djline.terminal=jline.UnixTerminal </code></pre>
- "Program arguments": <pre><code>--escapeOutput=true --csvDelimiter=, --showHeader=true --showLineNumbers=true --maxWidth=200 </code></pre>
- "Working directory": <pre><code> /home/amara/calcite/apache-calcite-1.22.0-src/example/csv</code></pre>

- Clicar no botão "Run"

> Após executar você verá:
<pre><code>
sqlline version 1.9.0
sqlline> 
</code></pre>


> Agora você pode testar:

- Carregue o modelo que está em (usando o driver que eu desenvolvi org.apache.calcite.jdbc.StreamDriver)
<pre><code>"src/test/resources/postgresql-foodmart-model.json" </code></pre>

<pre><code>sqlline> !connect jdbc:calcite:model=src/test/resources/postgresql-foodmart-model.json admin admin org.apache.calcite.jdbc.StreamDriver</code></pre>

> Após carregar o modelo você pode fazer consultas de teste:

<pre><code>0: jdbc:calcite:model=src/test/resources/post> SELECT STREAM * from ss2.people AS p;</code></pre>

### Resultado esperado: ###

<pre><code>
        +-------------------------+-----------+-------------+
        |         ROWTIME         | IDPEOPLE  |    NAME     |
        +-------------------------+-----------+-------------+
        | 2021-01-14 17:35:01.278 | 10        | Sales       |
        | 2021-01-14 17:35:05.285 | 20        | Marketing   |
        | 2021-01-14 17:35:05.285 | 426300172 | Accounts    |
        | 2021-01-14 17:35:05.286 | 40        | Sales       |
        | 2021-01-14 17:35:05.286 | 50        | Marketing   |
        | 2021-01-14 17:35:05.286 | 60        | Accounts    |
        | 2021-01-14 17:35:05.286 | 70        | setenta     |
        | 2021-01-14 17:35:05.286 | 426321278 | oitenta     |
        | 2021-01-14 17:35:05.286 | 90        | oitenta     |
        | 2021-01-14 17:35:05.286 | 100       | CEM         |
        | 2021-01-14 17:35:05.286 | 110       | CENTO E DEZ |
        +-------------------------+-----------+-------------+

</code></pre>

<pre><code>0: jdbc:calcite:model=src/test/resources/post> select * from GITREV.T as T where T."id" = 421862793;</code></pre>

> Essa consulta deve retornar vazio, pois você não tem o banco GITREV. Você pode configurar o acesso aos dados no arquivo 

<pre><code>/home/amara/calcite/apache-calcite-1.22.0-src/example/csv/src/test/resources/postgresql-foodmart-model.json</code></pre>